class SomeClass
  # Пример использования OrdersDetailsCalculator & OrdersPdfReport
  # предполагается что мы используем gem prawn

  def pdf_report
    shop = Shop.find(params[:id])
    report_params = params[:orders_report]
    from = (report_params[:from].present? && Date.parse(report_params[:from])) || 1.month.ago
    to = (report_params[:to].present? && Time.zone.parse(report_params[:to])) || Date.current

    # СОбираем данные в нужном формате
    orders_details_calculator = OrdersDetailsCalculator.new(shop, from: from, to: to)

    # Формируем на основе данных PDF документ
    pdf_report = OrdersPdfReport.new(orders_details_calculator)

    send_data pdf_report.document_content, filename: pdf_report.document_name, type: 'application/pdf'
  end
end