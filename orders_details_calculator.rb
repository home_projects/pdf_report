class OrdersDetailsCalculator
  attr_reader :date_from
  attr_reader :date_to
  attr_reader :result

  def initialize(shop, params = {})
    @shop = shop
    @date_from = params[:from]
    @date_to = params[:to] || Time.current
    @result = []

    calculate
  end

  def shop_name
    @shop.name
  end

  def calculate
    shop_orders = @shop.orders.includes(order_items: :item).where(date: @date_from..@date_to)

    return @result if shop_orders.none?

    # Хэш для быстрого получения uniqid юзера из таблицы UserShopRelations в цикле по заказам
    build_user_shop_hash

    # инфо по каждому товару
    shop_orders.find_each do |order|
      order.order_items.each do |order_item|
        @result << {
            item_uniqid:    order_item.item.uniqid,
            order_uniqid:   order.uniqid,
            amount:         order_item.amount,
            order_date:     order.date.strftime('%d.%m.%Y %H:%M:%S'),
            user_uniqid:    @user_shop_hash[order.user_id],
            recommended_by: order_item.recommended_by
        }
      end
    end

    @result
  end

  def build_user_shop_hash
    @user_shop_hash = {}
    @shop.user_shop_relations.find_each do |rel|
      @user_shop_hash[rel.user_id] = rel.uniqid
    end
    @user_shop_hash
  end
end
