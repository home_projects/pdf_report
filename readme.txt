# This is part of the code from commercial project.

We get shop data and use of these data for the report as a pdf document.

OrdersDetailsCalculator
     We get the data in the right format

OrdersPdfReport
     forming pdf

SomeClass
     a class where we use the classes described above.

---

Часть кода для формирования данных по магазинам и использования этих данных для отчета в виде pdf документа.

OrdersDetailsCalculator
    получаем данные в нужном формате

OrdersPdfReport
    формируем pdf

SomeClass
    некий класс где мы используем в работе описанные выше классы.