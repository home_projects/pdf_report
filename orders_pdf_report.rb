class OrdersPdfReport < Prawn::Document

  def initialize(calculator)
    @calculator = calculator
    super({})
  end

  def any_data?
    @calculator.result.any?
  end

  def document_content
    # русский шрифт
    font_families.update("Roboto" => {
        normal: "#{Rails.root}/lib/pdf/fonts/Roboto-Medium.ttf",
        bold:   "#{Rails.root}/lib/pdf/fonts/Roboto-Bold.ttf"
    })
    font 'Roboto', :size => 10

    image "#{Rails.root}/lib/pdf/images/logo.png", width: 30

    move_up 25
    text document_title, style: :bold, size: 12, color: '213242', align: :center
    move_down 40

    table_data = build_table_data

    table(table_data, header: true, row_colors: %w[f7f7f9 ffffff]) do
      row(0).style background_color: '083c6e', text_color: 'ffffff', border_color: 'e1e1e8'
      cells.style border_color: 'e1e1e8'
    end

    render
  end

  def document_name
    "REES46 - #{date_period_text('%d_%m_%Y', ' - ')}.pdf"
  end

  def document_title
    'Отчет за период: ' + date_period_text('%d.%m.%Y', ' - ')
  end

  def date_period_text(format, delimiter)
    [@calculator.date_from.strftime(format), @calculator.date_to.strftime(format)].join(delimiter)
  end

  def build_table_data
    # Шапка таблицы
    header = [ ['ID товара',  'ID заказа',  'Кол-во товара',  'Дата и время заказа',  'ID юзера',  'Тип рекомендера'] ]

    # Строки
    rows = []
    @calculator.result.each do |res|
      rows << [
          res[:item_uniqid],
          res[:order_uniqid],
          res[:amount],
          res[:order_date],
          res[:user_uniqid],
          res[:recommended_by]
      ].map(&:to_s)
    end

    header + rows
  end
end
